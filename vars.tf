variable "api_gateway_key" {
  description = "Key for API Gateway"
  type        = string
}
variable "api_gateway_rest_api" {
  description = "Name of API gateway Rest API"
  type        = string
}
variable "description" {
  type        = string
  default     = ""
  description = "The description of the REST API "
}
variable "binary_media_types" {
  type        = list
  default     = ["UTF-8-encoded"]
  description = "The list of binary media types supported by the RestApi. By default, the RestApi supports only UTF-8-encoded text payloads."
}
variable "minimum_compression_size" {
  type        = number
  default     = -1
  description = "Minimum response size to compress for the REST API. Integer between -1 and 10485760 (10MB). Setting a value greater than -1 will enable compression, -1 disables compression (default)."
}
variable "api_key_source" {
  type        = string
  default     = "HEADER"
  description = "The source of the API key for requests. Valid values are HEADER (default) and AUTHORIZER."
}
variable "types" {
  type        = list
  default     = ["EDGE"]
  description = "Whether to create rest api."
}
variable "stage_name" {
  type        = string
  default     = ""
  description = "The name of the stage. If the specified stage already exists, it will be updated to point to the new deployment. If the stage does not exist, a new one will be created and point to this deployment."
}
variable "domain_name" {
  type    = string
  default = ""
}
variable "cert_description" {
  type    = string
  default = ""
}
variable "path_parts" {
  type        = list
  default     = []
  description = "The last path segment of this API resource."
}
variable "stage_names" {
  type        = list
  default     = []
  description = "The name of the stage."
}
variable "http_methods" {
  type        = list
  default     = []
  description = "The HTTP Method (GET, POST, PUT, DELETE, HEAD, OPTIONS, ANY)."
}
variable "authorizations" {
  type        = list
  default     = []
  description = "The type of authorization used for the method (NONE, CUSTOM, AWS_IAM, COGNITO_USER_POOLS)."
}
variable "integration_types" {
  type        = list
  default     = []
  description = "The integration input's type. Valid values are HTTP (for HTTP backends), MOCK (not calling any real backend), AWS (for AWS services), AWS_PROXY (for Lambda proxy integration) and HTTP_PROXY (for HTTP proxy integration). An HTTP or HTTP_PROXY integration with a connection_type of VPC_LINK is referred to as a private integration and uses a VpcLink to connect API Gateway to a network load balancer of a VPC."
}
variable "variables" {
  type        = map
  default     = {}
  description = "A map that defines variables for the stage. ex: 'answer' = '42'"
}
variable "custom_domain_enabled" {
  type    = bool
  default = false
}
variable "regional_aws_acm_certificate_arn" {
  default = ""
}
variable "aws_route53_zone_id" {
  default = -1
}
variable "response_types" {
  type        = list
  default     = []
  description = "The response type of the associated GatewayResponse."
}
variable "status_codes" {
  type        = list
  default     = []
  description = "The HTTP status code."
}
variable "gateway_status_codes" {
  type        = list
  default     = []
  description = "The HTTP status code of the Gateway Response."
}
variable "gateway_response_templates" {
  type        = list
  default     = []
  description = "A map specifying the parameters (paths, query strings and headers) of the Gateway Response."
}
variable "gateway_response_parameters" {
  type        = list
  default     = []
  description = "A map specifying the templates used to transform the response body."
}
variable "cache_key_parameters" {
  type        = list
  default     = []
  description = "A list of cache key parameters for the integration."
}
variable "cache_namespaces" {
  type        = list
  default     = []
  description = "The integration's cache namespace."
}
variable "response_templates" {
  type        = list
  default     = []
  description = "A map specifying the templates used to transform the integration response body."
}
variable "gateway_response_count" {
  type        = number
  default     = 0
  description = "Number of count to create Gateway Response for api."
}
variable "metrics_enabled" {
  default = true
}
variable "logging_level" {
  default = "INFO"
}
variable "data_trace_enabled" {
  default = true
}
variable "model_count" {
  type        = number
  default     = 0
  description = "Number of count to create Model for api."
}
variable "model_names" {
  type        = list
  default     = []
  description = "The name of the model."
}
variable "model_descriptions" {
  type        = list
  default     = []
  description = "The description of the model."
}
variable "content_types" {
  type        = list
  default     = []
  description = "The content type of the model."
}
variable "validate_request_body" {
  default = true
}
variable "validate_request_parameters" {
  default = true
}
variable "api_gateway_request_validator_name" {
  type    = string
  default = ""
}
variable "stage_enabled" {
  type        = bool
  default     = false
  description = "Whether to create stage for rest api."
}
variable "deployment_enabled" {
  type        = bool
  default     = false
  description = "Whether to deploy rest api."
}
variable "api_gateway_usage_plan_name" {
  type    = string
  default = ""
  description = "This is the name of the Usage plan.Ex: my-usage-plan "
}
variable "api_gateway_usage_plan_description" {
  type        = string
  default     = ""
  description = "The description of the Usage Plan"
}
variable "product_code" {
  type        = string
  default     = ""
  description = "The AWS Marketplace product identifier to associate with the usage plan as a SaaS product on AWS Marketplace."
}
variable "limit" {
    type = number
    default = 20
}
variable "offset" {
    type = number
    default = 2
}
variable "period" {
    type = string
    default = "WEEK"
}
variable "burst_limit" {
    type = number
    default = 5
}
variable "rate_limit" {
    type = number
    default = 10
}
variable "vpc_link_count" {
  type        = number
  default     = 0
  description = "Number of count to create VPC Link for api."
}
variable "vpc_link_names" {
  type        = list
  default     = []
  description = "The name used to label and identify the VPC link."
}
variable "vpc_link_descriptions" {
  type        = list
  default     = []
  description = "The description of the VPC link."
}

variable "target_arns" {
  type        = list
  default     = []
  description = "The list of network load balancer arns in the VPC targeted by the VPC link. Currently AWS only supports 1 target."
}
